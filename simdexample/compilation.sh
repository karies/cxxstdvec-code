# iterate over architecture
g++ -msse4 -DUSE_VC -fabi-version=6 -std=c++11 -Ofast -I ${VCROOT} Contains.cxx -o test_Vc_sse4_gcc.x -ftree-vectorizer-verbose=2 -lrt -L ${VCROOT}/lib -lVc 2> Vclog
icc -msse4 -DUSE_SIMD -vec-report=2 -restrict -fabi-version=6 -std=c++11 -Ofast -I ${VCROOT} Contains.cxx -o test_SIMD_sse4_icc.x -lrt -L ${VCROOT}/lib -lVc 2> SIMDlog
icc -msse4 -DUSE_IVDEP -vec-report=2 -restrict -fabi-version=6 -std=c++11 -Ofast -I ${VCROOT} Contains.cxx -o test_IVDEP_sse4_icc.x -lrt -L ${VCROOT}/lib -lVc 2> IVDEPlog
icc -msse4 -vec-report=2 -restrict -fabi-version=6 -std=c++11 -Ofast -I ${VCROOT} Contains.cxx -o test_sse4_icc.x -lrt -L ${VCROOT}/lib -lVc 2> NORMALlog


