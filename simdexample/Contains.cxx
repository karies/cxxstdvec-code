 #include <cmath>
#include <iostream>
#include <random>
#include <cstdlib>
//#include "mm_malloc.h"

// a simple example to study #pragma simd
// compile with an intel compiler:
// icc -mavx -std=c++11  -vec-report=[2|3|4]  Contains.cxx -o foo.x -ltbb

#ifdef USE_VC
#include "Vc/vector.h"
#endif

double origin[3];
double boxsize[3];

// this is copied from the TBB implementation ( only tested on linux! )
struct StopWatch 
{
  long long now()
  {
    struct timespec ts;
    clock_gettime( CLOCK_REALTIME, &ts );
    return static_cast<long long>(1000000000UL)*static_cast<long long>(ts.tv_sec) + static_cast<long long>(ts.tv_nsec);
  }

  long long t1;
  long long t2;
  void Start(){  t1=now(); }
  void Stop(){  t2=now(); }
  double getDeltaSecs() { return (t2-t1)*1.E-9; }
};

#ifdef __INTEL_COMPILER
__declspec(vector) 
#endif
bool contains(double const* point)
{
  bool inside[3];
  for(unsigned int dir=0;dir < 3;dir++)
    {
      inside[dir] =  std::abs( point[dir] - origin[dir] ) < boxsize[dir]; 
    }
  return inside[0] & inside[1] & inside[2];
}


void contains_v(double const* __restrict__ points, bool * __restrict__ isin, int np)
{
#ifdef USE_SIMD
#  pragma simd  // simd forces; overrides "vectorization possible but inefficient"
#  pragma vector aligned
#endif
  for(unsigned int k=0; k<np; k++)
    {
      isin[k]=contains( &points[3*k] );
    }
}

void contains_v_inlined( double const* __restrict__ points, bool * __restrict__ isin, int np )
// void contains_v_inlined( double const*  points, bool *  isin, int np ) 
{
#ifdef USE_SIMD
#pragma simd  // simd forces; overrides "vectorization possible but inefficient"
#pragma vector aligned
#endif
#ifdef USE_IVDEP
#pragma ivdep
#pragma vector aligned
#endif
  for(unsigned int k=0; k<np; k++)
    {
      bool inside[3];
      for(unsigned int dir=0;dir < 3;dir++)
	{
	  inside[dir] = std::abs(points[3*k+dir] - origin[dir]) < boxsize[dir]; 
	}
      isin[k] = inside[0] & inside[1] & inside[2];
    }
}

void contains_v_inlined_unrolled( double const* __restrict__ points, bool * __restrict__ isin, int np ) 
{

#ifdef USE_SIMD
#pragma simd  // simd forces; overrides "vectorization possible but inefficient"
#pragma vector aligned
#endif
#ifdef USE_IVDEP
#pragma ivdep
#pragma vector aligned
#endif
  for(unsigned int k=0; k<np; k++)
    {
      bool inside[3];
      inside[0] =  std::abs( points[3*k] - origin[0] ) < boxsize[0]; 
      inside[1] =  std::abs( points[3*k+1] - origin[1] ) < boxsize[1]; 
      inside[2] =  std::abs( points[3*k+2] - origin[2] ) < boxsize[2]; 
      isin[k] = inside[0] & inside[1] & inside[2];
    }
}


#ifdef USE_VC
typedef Vc::double_v vd;
void contains_v_Vc( double const* __restrict__ points, bool * __restrict__ isin, int np )
{
  unsigned int const i[4]={0,3,6,9}; // "gather" indices
  Vc::uint_v const indices(i);

  for(unsigned int k = 0; k < np; k+= Vc::double_v::Size)
    {
      Vc::double_m mask[3];
      for(unsigned dir=0;dir < 3;dir++)
	{
	  vd x;x.gather(&points[3*k+dir],indices); // will copy a certain number of x's into vc vector x;
	  x=Vc::abs( x-origin[dir] );
	  mask[dir] = (x < boxsize[dir]);
	}

      Vc::double_m particleinside;
      particleinside = mask[0] && mask[1] && mask[2]; // we set it to one if inside
      for(int j=0;j<Vc::double_v::Size;++j){
	  isin[k+j]= particleinside[j];
	}
    }
}

typedef Vc::double_v vd;
void contains_v_Vc_unrolled( double const* points, bool *isin, int np )
{
  Vc::double_v const vcorigin[3]={origin[0],origin[1],origin[2]};
  unsigned int const i[4]={0,3,6,9}; // relative indices ( the length of 4 is implementation dependent )
  Vc::uint_v const indices(i);

  for(unsigned int k = 0; k < np; k+= Vc::double_v::Size)
    {
      vd x;x.gather(&points[3*k],indices); // will copy a certain number of x's into vc vector x;
      x=Vc::abs(x-vcorigin[0]);
      Vc::double_m c1 = (x < boxsize[0]);

      vd y;y.gather(&points[3*k+1],indices); // will copy a certain number of x's into vc vector x;
      y=Vc::abs(y-vcorigin[1]);
      Vc::double_m c2 = (y < boxsize[1]);

      vd z;z.gather(&points[3*k+2],indices); // will copy a certain number of x's into vc vector x;
      z=Vc::abs(z-vcorigin[2]);
      Vc::double_m c3 = (z < boxsize[2]);

      // this seems very inefficient
      Vc::double_m particleinside;
      particleinside = c1 && c2 && c3; // we set it to one if inside
      for(unsigned int j=0;j<Vc::double_v::Size;++j) isin[k+j]= particleinside[j];
    }
}
#endif

#ifdef CEAN

void contains_v_CEAN( double const* __restrict__ points, bool * __restrict__ isin, int np )
{
  const int N=4; // register size 
  for(int k = 0; k < np; k+= N)
    {
      const double (*p)[N*3] = (const double (*)[N*3]) &points[3*k];
      __assume_aligned(p,32);
      double in[N] __attribute__((aligned(32)));
      in[:]=1.;
      for(int dir=0;dir < 3;dir++)
	{
	  double x[N] __attribute__((aligned(32)));
	  
	  x[0]= p[0][dir];
	  x[1]= p[0][dir+3];
	  x[2]= p[0][dir+6];
	  x[3]= p[0][dir+9];
	 
	  // x[:]= p[0][dir, N, 3]; // gather components
	  std::cerr << k << " " << x[0] << " " << x[1] << " " << x[2] << " " << x[3] << std::endl;
	  std::cerr << k << " " << points[3*k+dir] << " " << points[3*k+dir+3] << " " << points[3*k+dir+6] << " " << points[3*k+dir+9] << std::endl;
	  std::cerr << k << " " << p[0][dir] << " " << p[0][dir+3] << " " << p[0][dir+6] << " " << p[0][dir+9] << std::endl;
	  in[:] *= ((std::abs( x[:] - origin[dir] )) < boxsize[dir] ) ? 0.0 : 1.0;
	}
      // cast result space as "local" CEAN array
      double (*visin)[N] = (double (*)[N]) &isin[3*k];
      visin[0][:]=(in[:]>0.)? true : false;
    }
  // some tail loop
}
#endif

int main()
{
  static double const L=10.;
  origin[0]=0.;
  origin[1]=0.;
  origin[2]=0.;
  boxsize[0]=L;
  boxsize[1]=L;
  boxsize[2]=L;

  int N=1000;

  // the mersenne twister 
  std::mt19937 rng;

  StopWatch tt;

  double * points = (double *) _mm_malloc(3*N*sizeof(double),32);
  bool * isin = (bool *) _mm_malloc(N*sizeof(bool),32);
  // fill points
  for(int i=0;i<N*3;i++)
    {
      points[i]=2.*L*(rng()/(1.*rng.max()) - 1.0);
    }

#ifdef USE_VC
  std::cerr << "executing Vc section with vector length " << Vc::double_v::Size << std::endl;
  tt.Start();
  for(int i=0;i<100;i++)
    {
      contains_v_Vc_unrolled(points, isin, N);
    }
  tt.Stop();
#endif
#ifdef CEAN
  std::cerr << "executing CEAN section with vector length " << std::endl;
  tt.Start();
  for(int i=0;i<100;i++)
    {
      contains_v_CEAN(points, isin, N);
    }
  tt.Stop();
#endif
#ifndef CEAN
  tt.Start();
  for(int i=0;i<100;i++)
    {
      #ifdef ORIG
      contains_v(points, isin, N);
      #endif
      #ifdef IUNROLLED
      contains_v_inlined_unrolled(points, isin, N);
      #endif
      #ifdef INLINED 
      contains_v_inlined(points, isin, N);
      #endif
    }
  tt.Stop();
#endif

  // to make sure compiler is not optimizing away the above function calls
  int count=0;
  for(int i=0;i<N;i++)
    {
      count+=isin[i];
    }

  std::cerr << tt.getDeltaSecs() << " " << count << std::endl;
}
